import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Developper } from '../models/dev.model';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { DevService } from '../services/dev.service';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-dev-list',
  templateUrl: './dev-list.component.html',
  styleUrls: ['./dev-list.component.css']
})

export class DevListComponent implements OnInit {
  displayedColumns: string[] = ['nom', 'prenom', 'langage', 'poste', 'experience',];
  //dataSource = new MatTableDataSource<Developper>(LIST_DEVS);
  devSubscription: Subscription;
  developpeurs: Developper[];
  dataSource;

  constructor(private router: Router, private devService: DevService, private authService: AuthService) { }
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  ngOnInit() {
    this.devSubscription = this.devService.devSubject.subscribe(
      (data: Developper[]) => {
        this.developpeurs = data;
        this.dataSource = new MatTableDataSource<Developper>(this.developpeurs);
        this.dataSource.paginator = this.paginator;
      }
    );

    this.devService.getAllDevs();
    this.devService.emit();
  }


  onAddDev() {
    this.router.navigate(['/developpers', 'add']);
  }

  public logout() {
    this.authService.signOutUser();
  }
}