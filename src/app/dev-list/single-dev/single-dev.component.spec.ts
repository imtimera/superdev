import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SingleDevComponent } from './single-dev.component';

describe('SingleDevComponent', () => {
  let component: SingleDevComponent;
  let fixture: ComponentFixture<SingleDevComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SingleDevComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SingleDevComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
