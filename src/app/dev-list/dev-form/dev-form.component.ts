import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DevService } from 'src/app/services/dev.service';
import { Developper } from 'src/app/models/dev.model';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-dev-form',
  templateUrl: './dev-form.component.html',
  styleUrls: ['./dev-form.component.css']
})
export class DevFormComponent implements OnInit,  OnDestroy {

  developpeurForm: FormGroup;
  devSubscription: Subscription;
  lastId: number;

  constructor(private formBuilder: FormBuilder,
    private router: Router, private devService: DevService) { }

  ngOnInit() {
    this.initForm();
    this.getAllDev();
  }

  getAllDev() {
    this.devSubscription = this.devService.devSubject.subscribe(
      (developpeurs: Developper[]) => {
        this.lastId = developpeurs.length;
      }
    );

    this.devService.getAllDevs();
    this.devService.emit();
  }


  initForm() {
    this.developpeurForm = this.formBuilder.group({
      nom: ['', Validators.required],
      prenom: ['', Validators.required],
      langage: ['', Validators.required],
      poste: ['', Validators.required],
      experience: ['', Validators.required]
    });
  }

  onBack() {
    this.router.navigate(['/developpeurs']);
  }
  onSubmit() {
    const id = this.lastId;
    const nom = this.developpeurForm.get('nom').value;
    const prenom = this.developpeurForm.get('prenom').value;
    const langage = this.developpeurForm.get('langage').value;
    const poste = this.developpeurForm.get('poste').value;
    const experience = this.developpeurForm.get('experience').value;
    const newDeveloppeur = new Developper(id, nom, prenom, langage, poste, experience);
    this.devService.addDev(newDeveloppeur);
    this.router.navigate(['/developpeurs']);
  }

  ngOnDestroy() {
    this.devSubscription.unsubscribe();
  }
}
