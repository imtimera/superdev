import { Injectable, OnInit } from '@angular/core';
import * as firebase from 'firebase'
@Injectable({
  providedIn: 'root'
})
export class AuthService  implements OnInit {
  isAuth: boolean;

  constructor() { }

  ngOnInit() {
    firebase.auth().onAuthStateChanged(
      (user) => {
        if (user) {
          this.isAuth = true;
        }
        else {
          this.isAuth = false;
        }
      }
    );
  }

  isAuthenticated() {
    return this.isAuth;
  }

  createNewUser(name: string, lastname: string, email: string, password: string, address: string, city: string) {
    return new Promise(
      (resolve, reject) => {
        firebase.auth().createUserWithEmailAndPassword(email, password)
        .then(
          ( ) => {
            const user = firebase.auth().currentUser;
            firebase.database().ref('users/' + user.uid).set({
              name: name,
              lastname: lastname,
              email: email,
              password: password,
              address: address,
              city: city
            }, function(error) {
              if (error) {
                // The write failed...
                console.error(error);
              } else {
                console.log('Data saved successfully!');
              }
            });
            resolve();
          },
          (error) => {
            reject(error);
          }
        );
      }
    );
  }


  signInUser(email: string, password:string){
    return new Promise(
      (resolve, reject) => {
        firebase.auth().signInWithEmailAndPassword(email, password)
        .then(
          () => {
            resolve();
          },
          (error) => {
            reject(error);
          }
        );
      }
    );
  }

  signOutUser(){
    firebase.auth().signOut();
  }
}
