import { Injectable } from '@angular/core';
import { Developper } from '../models/dev.model';
import { Subject } from 'rxjs';
import * as firebase from 'firebase';

@Injectable({
  providedIn: 'root'
})
export class DevService {

  devs: Developper[] = [];
  devSubject = new Subject<Developper[]>();

  emit() {
    this.devSubject.next(this.devs);
  }

  saveDev() {
    firebase.database().ref('/developpers').set(this.devs);
  }
  addDev(newDev: Developper) {
    this.devs.push(newDev);
    this.saveDev();
    this.emit();
  }

  getAllDevs() {
    firebase.database().ref('/developpers')
      .on('value', (data) => {
          this.devs = data.val() ? data.val() : [];
          this.emit();
        }
      );
  }

  getDevById(id: number) {
    return new Promise(
      (resolve, reject) => {
        firebase.database().ref('/developpers/' + id).once('value').then(
          (data) => {
            resolve(data.val());
          }, (error) => {
            reject(error);
          }
        );
      }
    );
  }
}
