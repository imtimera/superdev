import { Component } from '@angular/core';
import * as firebase from 'firebase';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'super-dev';

  constructor() {
    const firebaseConfig  = {
      apiKey: "AIzaSyA8HO8tYV5Onn3_okO5Pwnw2C-nkgw_kRE",
      authDomain: "super-dev-278b8.firebaseapp.com",
      databaseURL: "https://super-dev-278b8.firebaseio.com",
      projectId: "super-dev-278b8",
      storageBucket: "",
      messagingSenderId: "556630503438",
      appId: "1:556630503438:web:c38ae9dba08615eee14c5f",
      measurementId: "G-4BVNSHSGZ7"

    };
    firebase.initializeApp(firebaseConfig );
  }
}
