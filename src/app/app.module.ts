import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { SigninComponent } from './auth/signin/signin.component';
import { SignupComponent } from './auth/signup/signup.component';
import { SingleDevComponent } from './dev-list/single-dev/single-dev.component';
import { DevFormComponent } from './dev-list/dev-form/dev-form.component';
import { AppRoutingModule } from './app-routing.module';
import { DevListComponent } from './dev-list/dev-list.component';
import { AuthService } from './services/auth.service';
import { DevService } from './services/dev.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
  MatTableModule,
  MatPaginatorModule,
  MatSortModule,
  MatProgressSpinnerModule,
  MatFormFieldModule,
  MatCardModule,
  MatInputModule,
  MatButtonModule,
  MatIconModule,
  MatToolbarModule

} from '@angular/material';

@NgModule({
  declarations: [
    AppComponent,
    SigninComponent,
    SignupComponent,
    SingleDevComponent,
    DevFormComponent,
    DevListComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatProgressSpinnerModule,
    MatFormFieldModule,
    MatCardModule,
    FormsModule,
    MatInputModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatIconModule,
    MatToolbarModule,

  ],
  providers: [AuthService, DevService],
  bootstrap: [AppComponent]
})
export class AppModule { }
