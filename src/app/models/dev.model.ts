export class Developper {
    constructor(public id: number, public nom: string, public prenom: string,
                public langage: string, public poste: string, public experience: number) {
    }
}
