import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DevListComponent } from './dev-list/dev-list.component';
import { SingleDevComponent } from './dev-list/single-dev/single-dev.component';
import { DevFormComponent } from './dev-list/dev-form/dev-form.component';
import { SigninComponent } from './auth/signin/signin.component';
import { SignupComponent } from './auth/signup/signup.component';
import { AuthGuardService } from './services/auth-guard.service';

const routes: Routes = [
  { path: 'auth/signup', component: SignupComponent },
  { path: 'auth/signin', component: SigninComponent },
  { path: 'developpers', canActivate: [AuthGuardService], component: DevListComponent },
  { path: 'developpers/add', canActivate: [AuthGuardService], component: DevFormComponent },
  { path: 'developpers/:id', canActivate: [AuthGuardService], component: SingleDevComponent },
  { path: '', redirectTo: '/developpers', pathMatch: 'full'  },
  { path: '**', redirectTo: '/developpers' }

];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})

export class AppRoutingModule { }
