import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  signUpForm: FormGroup;
  errorMessage: string;

  constructor(private formBuilder: FormBuilder,
    private authService: AuthService,
    private router: Router) { }

  ngOnInit() {
    this.initForm();
  }


  initForm() {
    this.signUpForm = this.formBuilder.group({
      name: ['', [Validators.required, Validators.minLength(3)]],
      lastname: ['', [Validators.required, Validators.minLength(3)]],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.pattern(/[0-9a-zA-Z]{6,}/)]],
      address: ['', []],
      city: ['', []],
    });
  }

  onSubmit() {
    const name = this.signUpForm.get('name').value;
    const lastname = this.signUpForm.get('lastname').value;
    const email = this.signUpForm.get('email').value;
    const password = this.signUpForm.get('password').value;
    const address = this.signUpForm.get('address').value;
    const city = this.signUpForm.get('city').value;
    this.authService.createNewUser(name, lastname, email, password, address, city).then(
      () => {
        this.router.navigate(['/developpers']);
      }, (error) => {
        this.errorMessage = error;
      }
    );

  }


}
